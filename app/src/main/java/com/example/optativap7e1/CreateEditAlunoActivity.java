package com.example.optativap7e1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.ArrayList;

import aluno.Aluno;
import aluno.AlunoDB;

public class CreateEditAlunoActivity extends AppCompatActivity {

    EditText nomeAlunoET, emailAlunoET, telefoneAlunoET, idadeAlunoET;
    Button saveAlunoBTN, cancelBTN;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_edit_aluno);

        nomeAlunoET = findViewById(R.id.nomeAlunoET);
        emailAlunoET = findViewById(R.id.emailAlunoET);
        telefoneAlunoET = findViewById(R.id.telefoneAlunoET);
        idadeAlunoET = findViewById(R.id.idadeAlunoET);

        saveAlunoBTN = findViewById(R.id.saveAlunoBtn);
        cancelBTN = findViewById(R.id.cancelBtn);

        cancelBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        saveAlunoBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveAluno();
            }
        });

    }

    public void saveAluno() {
        String nome = nomeAlunoET.getText().toString();
        String email = emailAlunoET.getText().toString();
        String telefone = telefoneAlunoET.getText().toString();
        int idade = idadeAlunoET.getText().toString().equals("") ? 0 : Integer.parseInt(idadeAlunoET.getText().toString());

        AlunoDB alunoDB = new AlunoDB(this, "", null, 1);

        alert(idadeAlunoET.getText().toString());

        if(!nome.equals("") && !email.equals("") && !telefone.equals("") && idade != 0) {
            if(alunoDB.create(nome, email, telefone, idade)) {
                alert("Aluno Salvo com sucesso");
                finish();
            } else {
                alert("Ocorreu um erro ao salvar o aluno");
            }
        } else {
            alert("Dados faltando");
        }
    }

    public void alert(String msg) {
        Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
    }
}
