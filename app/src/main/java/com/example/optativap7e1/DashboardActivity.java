package com.example.optativap7e1;

import android.content.Intent;
import android.os.Parcelable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.Serializable;

import user.User;

public class DashboardActivity extends AppCompatActivity {

    TextView cadastrarET, listarET, configsET, editarET;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);

        cadastrarET = findViewById(R.id.cadastrarBtn);
        listarET = findViewById(R.id.listarBtn);
        configsET = findViewById(R.id.configsBtn);
        editarET = findViewById(R.id.editarBtn);

        final Intent intent = getIntent();
        final User loggedUser = new User(Integer.parseInt(intent.getStringExtra("id")), intent.getStringExtra("nome"), intent.getStringExtra("username"));

        configsET.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent configs = new Intent(getApplicationContext(), CreateUserActivity.class);
                configs.putExtra("id", loggedUser.getId());
                configs.putExtra("nome", loggedUser.getNome());
                configs.putExtra("username", loggedUser.getUsername());
                startActivity(configs);
            }
        });

        cadastrarET.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent cadastrar = new Intent(getApplicationContext(), CreateEditAlunoActivity.class);
                startActivity(cadastrar);
            }
        });

        listarET.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), ListarAlunosActivity.class));
            }
        });

        editarET.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(), "Clique no botão editar...", Toast.LENGTH_LONG).show();
                startActivity(new Intent(getApplicationContext(), ListarAlunosActivity.class));
            }
        });

    }
}
