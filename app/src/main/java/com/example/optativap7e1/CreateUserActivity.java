package com.example.optativap7e1;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import user.UserDB;

public class CreateUserActivity extends AppCompatActivity {

    TextView passTV;
    EditText nomeET, userET, passET, confirmPassET;
    Button createBTN, cancelBTN;
    int id;
    boolean isToUpdate = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_user);

        passTV = findViewById(R.id.passTV);

        cancelBTN = findViewById(R.id.cancelBtn);
        createBTN = findViewById(R.id.createBtn);

        nomeET = findViewById(R.id.nomeET);
        userET = findViewById(R.id.userET);
        passET = findViewById(R.id.passET);

        Intent intent = getIntent();

        if(intent.getIntExtra("id", 0) != 0) {
            nomeET.setText(intent.getStringExtra("nome"));
            userET.setText(intent.getStringExtra("username"));
            id = intent.getIntExtra("id", 0);

            LinearLayout linearLayout = findViewById(R.id.linearLayout);
            TextView confirmPasswordTV = new TextView(this);
            EditText confirmPasswordET = new EditText(this);
            passTV.setText("Nova Senha");
            confirmPasswordTV.setText("Confirme a Senha");
            linearLayout.addView(confirmPasswordTV);
            linearLayout.addView(confirmPasswordET);

            confirmPassET = confirmPasswordET;
            createBTN.setText("ATUALIZAR");

            isToUpdate = true;
        }

        cancelBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        createBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isToUpdate) {
                    updateUser();
                } else {
                    createUser();
                }
            }
        });
    }

    public void updateUser() {
        String nome = nomeET.getText().toString();
        String user = userET.getText().toString();
        String pass = passET.getText().toString();
        String confirmPass = confirmPassET.getText().toString();

        UserDB userDB = new UserDB(this, "", null, 1);

        if(!nome.equals("") && !user.equals("") && !pass.equals("") && !confirmPass.equals("") && (pass.equals(confirmPass))) {
            if(userDB.updateUser(id, nome, user, pass)) {
                alert("Dados atualizados com sucesso");
                finish();
            } else {
                alert("Ocorreu um erro ao atualizar o usuário");
            }
        } else {
            if(!pass.equals(confirmPass)) {
                alert("As senha não conferem");
            } else {
                alert("Dados faltando");
            }
        }
    }

    public void createUser() {
        String nome = nomeET.getText().toString();
        String user = userET.getText().toString();
        String pass = passET.getText().toString();

        UserDB userDB = new UserDB(this, "", null, 1);
        if(!nome.equals("") && !user.equals("") && !pass.equals("")) {
            if (userDB.createUser(nome, user, pass)) {
                alert("Usuário criado com sucesso\nVocê pode logar agora");
                finish();
            } else {
                alert("Ocorreu um erro ao criar o usuário");
            }
        }
    }

    public void alert(String msg) {
        Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
    }
}
