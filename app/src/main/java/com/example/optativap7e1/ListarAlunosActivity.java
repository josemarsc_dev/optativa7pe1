package com.example.optativap7e1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;

import aluno.Adapter;
import aluno.Aluno;
import aluno.AlunoDB;

public class ListarAlunosActivity extends AppCompatActivity {

    private ListView listView;
    private ArrayList<Aluno> alunos = new ArrayList<>();
    private Adapter alunoAdapter;

    AlunoDB alunoDB = new AlunoDB(this, "", null, 1);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listar_alunos);

        listView = findViewById(R.id.listView);
        alunos = alunoDB.select();
        alunoAdapter = new Adapter(this, alunos);
        listView.setAdapter(alunoAdapter);
    }
}
