package com.example.optativap7e1;

import android.content.Intent;
import android.os.Parcelable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.Toast;

import user.User;
import user.UserDB;

public class MainActivity extends AppCompatActivity {

    EditText userET, passET;
    Button loginBtn, createBtn;
    Switch showPass;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        startDB();

        userET = findViewById(R.id.userET);
        passET = findViewById(R.id.passET);
        loginBtn = findViewById(R.id.loginBtn);
        createBtn = findViewById(R.id.createBtn);

        showPass = findViewById(R.id.showPass);

        loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                login();
            }
        });
        createBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent createUser = new Intent(getApplicationContext(), CreateUserActivity.class);
                startActivity(createUser);
            }
        });

        showPass.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked) {
                    passET.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                } else {
                    passET.setTransformationMethod(PasswordTransformationMethod.getInstance());
                }
            }
        });

    }

    public void startDB() {
        UserDB userDB = new UserDB(this, "", null, 1);
    }

    public void login() {
        String user = userET.getText().toString();
        String pass = passET.getText().toString();

        if(user.equals("") || pass.equals("")) {
            alert("Dados faltando");
        } else {

            UserDB userDB = new UserDB(this, "", null, 1);

            User loggedUser = userDB.getUser(user, pass);

            if (loggedUser == null) {
                alert("Usuário ou senha incorretos");
            } else {
                Intent dashboard = new Intent(getApplicationContext(), DashboardActivity.class);
                dashboard.putExtra("id", Integer.toString(loggedUser.getId()));
                dashboard.putExtra("nome", loggedUser.getNome());
                dashboard.putExtra("username", loggedUser.getUsername());
                startActivity(dashboard);
            }
        }
    }

    public void alert(String msg) {
        Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
    }
}
