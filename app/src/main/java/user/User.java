package user;

public class User {
    int id;
    String nome;
    String username;
    String password;

    public User() {

    }

    public User(String nome, String username) {
        this.nome = nome;
        this.username = username;
        this.password = password;
    }

    public User(int id, String nome, String username) {
        this.id = id;
        this.nome = nome;
        this.username = username;
        this.password = password;
    }

    public int getId() {
        return id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
