package user;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class UserDB extends SQLiteOpenHelper {

    private static final String dnName = "optativap7e1.db";

    public UserDB(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, dnName, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String sql = "CREATE TABLE user(id INTEGER PRIMARY KEY AUTOINCREMENT, nome TEXT, username TEXT, password TEXT)";
        String sql2 = "CREATE TABLE aluno(id INTEGER PRIMARY KEY AUTOINCREMENT, nome TEXT, email TEXT, telefone TEXT, idade INTEGER)";
        db.execSQL(sql);
        db.execSQL(sql2);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS user");
        onCreate(db);
    }

    public User getUser(String username, String password) {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM user", null);
        while (cursor.moveToNext()) {
            Log.i("GETUSER 0", cursor.getString(0));
            Log.i("GETUSER 1", cursor.getString(1));
            Log.i("GETUSER 2", cursor.getString(2));
            Log.i("GETUSER 3", cursor.getString(3));
            if(username.equals(cursor.getString(2)) && password.equals(cursor.getString(3))) {
                User user = new User(Integer.parseInt(cursor.getString(0)), cursor.getString(1), cursor.getString(2));
                return  user;
            }
        }
        return null;
    }

    public boolean updateUser(int id, String nome, String username, String password) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("nome", nome);
        contentValues.put("username", username);
        contentValues.put("password", password);

        db.update("user", contentValues, "id = ?", new String[]{Integer.toString(id)});
        return true;
    }

    public boolean createUser(String nome, String username, String password) {
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            ContentValues contentValues = new ContentValues();
            contentValues.put("nome", nome);
            contentValues.put("username", username);
            contentValues.put("password", password);

            db.insert("user", null, contentValues);
            return true;
        } catch (SQLiteException ex) {
            return false;
        }

    }
}
