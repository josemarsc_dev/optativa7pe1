package aluno;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;

public class AlunoDB extends SQLiteOpenHelper {

    private static final String dnName = "optativap7e1.db";

    public AlunoDB(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, dnName, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS aluno");
        onCreate(db);
    }

    public ArrayList<Aluno> select() {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM aluno", null);
        ArrayList<Aluno> alunos = new ArrayList<>();
        while (cursor.moveToNext()) {
//            Log.i(cursor.getColumnName(0), cursor.getString(0));
//            Log.i(cursor.getColumnName(1), cursor.getString(1));
//            Log.i(cursor.getColumnName(2), cursor.getString(2));
//            Log.i(cursor.getColumnName(3), cursor.getString(3));
//            Log.i(cursor.getColumnName(4), cursor.getString(4));
            Aluno aluno = new Aluno(Integer.parseInt(cursor.getString(0)), cursor.getString(1), cursor.getString(2), cursor.getString(3), cursor.getInt(4));
            alunos.add(aluno);
        }
        return alunos;
    }

    public boolean update(int id, String nome, String email, String telefone, int idade) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("nome", nome);
        contentValues.put("email", email);
        contentValues.put("telefone", telefone);
        contentValues.put("idade", idade);

        db.update("aluno", contentValues, "id = ?", new String[]{Integer.toString(id)});
        return true;
    }

    public boolean create(String nome, String email, String telefone, int idade) {
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            ContentValues contentValues = new ContentValues();
            contentValues.put("nome", nome);
            contentValues.put("email", email);
            contentValues.put("telefone", telefone);
            contentValues.put("idade", idade);

            db.insert("aluno", null, contentValues);
            return true;
        } catch (SQLiteException ex) {
            return false;
        }

    }

    public boolean delete(String id) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete("aluno", "id = ?", new String[]{id});
        return true;
    }
}
