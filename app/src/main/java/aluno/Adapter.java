package aluno;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.optativap7e1.R;

import java.util.ArrayList;

public class Adapter extends BaseAdapter {
    Activity context;
    ArrayList<Aluno> alunos;
    private static LayoutInflater inflater = null;

    public Adapter(Activity context, ArrayList<Aluno> alunos) {
        this.context = context;
        this.alunos = alunos;

        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return alunos.size();
    }

    @Override
    public Object getItem(int position) {
        return alunos.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View itemView = convertView;
        itemView = (itemView == null) ? inflater.inflate(R.layout.aluno_item_view, null) : itemView;

        TextView nomeLV = (TextView) itemView.findViewById(R.id.nomeLV);
        TextView emailLV = (TextView) itemView.findViewById(R.id.emailLV);
        TextView telefoneLV = (TextView) itemView.findViewById(R.id.telefoneLV);
        TextView idadeLV = (TextView) itemView.findViewById(R.id.idadeLV);

        Aluno selectedAluno = alunos.get(position);

        nomeLV.setText(selectedAluno.getNome());
        emailLV.setText(selectedAluno.getEmail());
        telefoneLV.setText(selectedAluno.getTelefone());
        idadeLV.setText(selectedAluno.getIdade());

        return null;
    }
}
